# `gpx2png`
This programs converts `.gpx` files (GPS eXchange format) into PNG
images by using images tiles from the OpenStreetMap project
(http://osm.org) and drawing sequences of lines corresponding to GPS
track points or circles for each way point.

# Licence
This code is released under the GNU Public Licence version 3 or any later version.

Copyright 2009-2014 Thomas Fischer <fischer@unix-ag.uni-kl.de>
Copyright 2020 Grégory David <dev@groolot.net>

# Usage
```
gpx2png.pl [OPTIONS] GPXFILE [GPXFILES]
```

GPS tracks (GPS eXchange format: `.gpx` file) are passed as a list of
filenames, or the `.gpx` files' content is piped into `gpx2png.pl`

# Options
## `-T|--tilesdir DIRNAME`
Output directory for downloaded tiles. Default: `./tiles`

## `-o|--output FILENAME`
Output filename of the image. Default: `map.png`

## `-z|--zoom N`
Zoom level (number or "auto"). Default: auto

## `-a|--autozoom N`
Do not use more than N tiles to draw tracks. Default: 32

## `-b|--bordertiles N`
Additional map image tiles around the map. Default: 0

## `-c|--cutborder N`
Cut final map to have N pixels around the drawn tracks. Default: 0

## `-r|--waypointradius N`
Radius for waypoint circles. Default: auto

## `-w|--linewidth N`
Stroke line width. Default: 5

## `-A|--animate`
Create animation steps by saving individual images for each drawn
track. Default: off

## `-t|--tiles SOURCE`
Select the source of image tiles. Possible values for `SOURCE`:
 - standard (default)
 - cyclemap
 - transport
 - opnvkarte
 - mapquest
 - toner
 - toner-lines
 - watercolor
 - thunderforest
 - hikebike
 - hillshading (transparent!)
 - white (no tiles, uses grayscale drawing)
 - transparent (no tiles, uses grayscale drawing)

## `--backgroundpostprocess MODE`
Post-process the background (e.g. tiles) by changing saturation and
brightness. Possible values for `MODE`:
 - normal (default)
 - bright
 - brightgrey
 - grey

## `-s|--sparse`
Sparse mode: Include only tiles touched by GPS tracks. Default: off

## `-q|--quiet`
Quiet mode: Do not print status output. Default: off

## `-j|--thumbnail FILENAME`
Show thumbnail of JPEG photo at the position as determined by GPS
coordinates in EXIF tag. Multiple `-j` options possible

## `-W|--invisiblewaypoint n.n:n.n`
Additional invisible point to be included in the map. Format: decimal
latitude, colon/comma, decimal longitude

## `-B|--boudingbox n:n:n:n`
Enforce a bounding box. Format: decimal min latitude, colon/comma, min
longitude, colon/comma, max latitude, colon/comma, max longitude

## `-J|--thumbnailsize N`
Set size of JPEG photo thumbnails. Default: 128

## `-i|--trackicon FILENAME`
Draw icons along a track like a dotted line. Icons are rotated towards
track's direction, up is forward.

## `-I|--trackicondist N`
Distance between the center of two subsequent track icons. Default: 20

# Legacy
See https://tfischernet.wordpress.com/2009/05/04/drawing-gps-traces-on-map-tiles-from-openstreetmap/
and http://wiki.openstreetmap.org/wiki/Gpx2png
